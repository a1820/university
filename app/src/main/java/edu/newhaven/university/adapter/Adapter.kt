package edu.newhaven.university.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import edu.newhaven.university.MainActivity
import edu.newhaven.university.R
import edu.newhaven.university.viewModel.MainViewModel
import edu.newhaven.university.databinding.DataBinding

class Adapter(private val context:Context, private val arrayList: ArrayList<MainViewModel>): RecyclerView.Adapter<Adapter.CustomView>() {

    //View Holder for Adapter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        val layoutInflater = LayoutInflater.from(parent.context)

        //Binding data to item_cell layout
        val dataBinding:DataBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_cell, parent , false)

        return CustomView(dataBinding)
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {

        //Toast to show at the end of the adapter list
        if(position==itemCount-1){
            Toast.makeText(context,"This is The end",Toast.LENGTH_LONG).show();
        }
        val viewModel = arrayList[position]
        holder.bind(viewModel)
    }

    override fun getItemCount(): Int {
       return arrayList.size
    }

    //Cutom view bind data
    class CustomView(val dBinding: DataBinding):RecyclerView.ViewHolder(dBinding.root){

        fun bind (viewModel : MainViewModel){
            this.dBinding.model = viewModel
            dBinding.executePendingBindings()
        }
    }

}
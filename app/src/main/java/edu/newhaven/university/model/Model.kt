package edu.newhaven.university.model

//Model Class for the data from the endpoint
class Model {
    var msg = ""
    var res = arrayOf<SubModel>()
}

//Sub Model for the res array in the endpoint
class SubModel{
    var name = ""
    var image_url =""
 //   var web_pages = ""
    var country = ""
    constructor(name: String, image_url: String,  country: String) {
        this.name = name
        this.image_url = image_url
  //    this.web_pages = web_pages
        this.country = country
    }
}

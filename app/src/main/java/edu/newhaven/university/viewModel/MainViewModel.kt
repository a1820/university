package edu.newhaven.university.viewModel


import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import edu.newhaven.university.MainActivity
import edu.newhaven.university.model.Model
import edu.newhaven.university.model.SubModel
import edu.newhaven.university.network.APIinterface
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// View Modal
class MainViewModel: ViewModel {

    var name = ""
    var image_url = ""
   // var web_pages = ""
    var country = ""

    constructor() : super()

    //Intialzing view model data
    constructor(model: SubModel) : super() {
        this.name = model.name
        this.image_url = model.image_url
      //  this.web_pages = model.web_pages
        this.country = model.country
    }

    //Creating MutableLiveData Array list of the objects defined
    var arrayListML = MutableLiveData<ArrayList<MainViewModel>>()
    var arrList = ArrayList<MainViewModel>()

    //var arrName  = arrayListOf<String>("Network Unavaliable")

    fun getImageUrl(): String{
        return image_url
    }

    //Get the arrayList from API end call
    fun getArrList(): MutableLiveData<ArrayList<MainViewModel>>{

        //Using retrofit to get the data
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://3.128.30.248:3000/data/")
            .build()
            .create(APIinterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object: Callback<Model> {
            override fun onResponse(call: Call<Model>, response: Response<Model>) {
                val responseBody = response.body()
                if(response.isSuccessful){
                    if (responseBody != null) {
                       // Log.d("Hello", Gson().toJson(responseBody.res))
                        for(i in 0..99 ) {
                            val a = responseBody.res[i].name
                            val image = responseBody.res[i].image_url
                            val country = responseBody.res[i].country
                            //  Log.d("Array1", responseBody.res[i].name)
                            // arrName.add(a)
                            val n1 = SubModel(a, image, country)
                            val mainViewModel1: MainViewModel = MainViewModel(n1);
                            arrList!!.add(mainViewModel1);
                            arrayListML.value = arrList
                        }
                    }
                }
            }
            override fun onFailure(call: Call<Model>, t: Throwable) {
                Log.d("TAG", "onFailure" + t.message)
            }
        })

        // for ( i in 0..arrName.size-1){
        // val n1 = Model("AjiethVenkat")
        // val n2 = Model("Giri")
        // val mainViewModel2: MainViewModel = MainViewModel(n2);
        // arrList!!.add(mainViewModel2);

        return arrayListML
    }
}

//Setting Image for url using Picassco Library
object ImageBindingAdapter {

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageUrl(view: ImageView, url: String){
            Picasso.get().load(url).into(view)
    }
}

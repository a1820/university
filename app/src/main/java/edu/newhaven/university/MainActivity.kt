package edu.newhaven.university

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.newhaven.university.adapter.Adapter
import edu.newhaven.university.viewModel.MainViewModel

class MainActivity : AppCompatActivity() {

    //Intializing vriables for Recycler and Adapter
    private var recyclerview: RecyclerView? = null
    private  var customAdapter: Adapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        recyclerview = findViewById(R.id.RecyclerView) as RecyclerView

        var mainViewModel: MainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        //Observer for data from view models
        mainViewModel.getArrList().observe(this, Observer {mainViewModels->

            //Setting up Custom Adapter
            customAdapter = Adapter(this@MainActivity, mainViewModels!!)
            recyclerview!!.layoutManager = LinearLayoutManager(this@MainActivity)
            recyclerview!!.adapter = customAdapter
        })
    }
}



package edu.newhaven.university.network

import edu.newhaven.university.model.Model
import retrofit2.Call
import retrofit2.http.GET

//Interface for the retrofit API to make a GET call using the model
interface APIinterface {

    @GET("universities")
    fun getData(): Call<Model>
}